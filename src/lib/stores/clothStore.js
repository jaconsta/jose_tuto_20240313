import { writable } from 'svelte/store';

function createClothStore(initialValue) {
	const { subscribe, set, update } = writable(initialValue);

	return {
		subscribe,
		set,
		append: (newCloth) => update((clothes) => [...clothes, newCloth]),
		reset: () => set([]),
		pop: () =>
			update((clothes) => {
				const copyCloth = [...clothes];
				copyCloth.pop();
				return copyCloth;
			})
	};
}

export const clothStore = createClothStore([]);
